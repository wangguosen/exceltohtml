package com.tansun.exceltohtml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ExceltohtmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceltohtmlApplication.class, args);
	}

}
