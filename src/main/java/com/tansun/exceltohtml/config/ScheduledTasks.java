package com.tansun.exceltohtml.config;

import com.tansun.exceltohtml.util.Constants;
import com.tansun.exceltohtml.util.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 作者：WGS
 * Q  Q：1150111308
 * 邮箱：gosse0405@163.com
 */
@Component
public class ScheduledTasks {

	@Scheduled(cron = "0 59 23 ? * *")
	public void reportCurrentTime() {
		FileUtil.delete(FileUtil.getDateDir());
	}

}
