package com.tansun.exceltohtml.controller;

import com.tansun.exceltohtml.util.Constants;
import com.tansun.exceltohtml.util.ExcelToHtmlUtil;
import com.tansun.exceltohtml.util.FileUtil;
import com.tansun.exceltohtml.util.JsonResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 作者：WGS
 * Q  Q：1150111308
 * 邮箱：gosse0405@163.com
 */
@Controller
public class ExcelController {

	@RequestMapping("/")
	public String index() {
		return "excel.html";
	}

	@RequestMapping("/upload")
	@ResponseBody
	public JsonResult upload(@RequestParam("file") MultipartFile file) {
		return FileUtil.upload(file);
	}

	@RequestMapping("/download")
	public void upload(String fileName, HttpServletResponse response) {
		FileUtil.download(FileUtil.getDateDir() + File.separator + fileName, response);
	}

	@RequestMapping("/toHtml")
	public void toHtml(HttpServletResponse response, String fileName, int sheetNo) {
		try {
			String s = ExcelToHtmlUtil.excelToHtml(new File(Constants.UPLOAD_DIR + FileUtil.getDateDir() + File.separator + fileName), sheetNo);
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().write(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/sheetList")
	@ResponseBody
	public JsonResult sheetList(String fileName) {
		return JsonResult.ok("上传成功").setData(ExcelToHtmlUtil.getSheetList(new File( Constants.UPLOAD_DIR  + FileUtil.getDateDir() + File.separator +  fileName)));
	}
}
