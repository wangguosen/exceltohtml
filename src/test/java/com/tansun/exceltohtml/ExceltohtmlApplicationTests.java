package com.tansun.exceltohtml;

import com.tansun.exceltohtml.util.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;

@SpringBootTest
class ExceltohtmlApplicationTests {

	@Test
	void readDate() throws IOException {
		String srcPath="D:\\poi\\dateTest2.xlsx";
		XSSFWorkbook hw = new XSSFWorkbook(new FileInputStream(srcPath));;
		Sheet sheet = hw.getSheetAt(0);
		for (int i = 0; i < sheet.getLastRowNum() + 1; i++) {
			Cell cell = sheet.getRow(i).getCell(0);
			System.out.println("===========================================");
			System.out.println("第" + (i+1) + "个" + getCellValue(cell));
		}
	}

	/**
	 * 获取单元格数据
	 * @param cell
	 * @return
	 */
	private String getCellValue(Cell cell) {
		String result = "";
		if (cell == null) {
			return "";
		} else {
			switch (cell.getCellType()) {
			case NUMERIC:
				String dataFormatString = cell.getCellStyle().getDataFormatString(); // 获取单元格格式内容
				short dataFormat = cell.getCellStyle().getDataFormat(); // 获取单元格格式ID
				if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) { // 判断内置日期时间类型
					if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) {
						result = DateUtil.parseDateToStr(cell.getDateCellValue(), "HH:mm");
					} else {
						result = DateUtil.parseDateToStr(cell.getDateCellValue(), "yyyy-MM-dd");
					}
				} else if (dataFormat == 31 || dataFormat == 57) {
					result = DateUtil.parseDateToStr(cell.getDateCellValue(), "yyyy-MM-dd");
				} else {
					result = new DataFormatter().formatCellValue(cell);
				}
				break;
			case STRING:
				result = cell.getRichStringCellValue().toString();
				break;
			case FORMULA:
				try {
					// 处理科学计数法
					NumberFormat nf = NumberFormat.getInstance();
					// 是否以逗号隔开, 默认true以逗号隔开,如[123,456,789.128]
					nf.setGroupingUsed(false);
					result = String.valueOf(nf.format(cell.getNumericCellValue()));
				} catch (IllegalStateException e) {
					result = String.valueOf(cell.getRichStringCellValue());
				}
				break;
			case ERROR: //故障
				result = "非法字符";
				break;
			}
			return result;
		}

	}

}
